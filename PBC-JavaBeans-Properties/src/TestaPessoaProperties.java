import java.util.List;

import log.Log;
import model.Pessoa;
import ouvintes.ChangeOuvinte;

public class TestaPessoaProperties {

    public static void main(String[] args) {

        Pessoa pessoa=new Pessoa();
        ChangeOuvinte ouvinte=new ChangeOuvinte();

        pessoa.addPropertyChangeListener(ouvinte);


        pessoa.setNome("Jose");
        pessoa.setNome("Jose Maria");
        pessoa.setNome("Jose Maria Soares");


        List<Log> logs=ouvinte.getLogList();

        for(Log log:logs){
            System.out.println(log);
        }

    }
}
