import java.beans.PropertyVetoException;

import model.Pessoa;
import ouvintes.ChangeOuvinte;
import ouvintes.VetoOuvinte;

public class TestaPessoaVeto {

    public static void main(String[] args) {

        Pessoa pessoa=new Pessoa();
        VetoOuvinte ouvinte=new VetoOuvinte();

        pessoa.addPropertyVetoableChangeListener(ouvinte);


        pessoa.setNome("Jose");

        try {
            pessoa.setIdade(250); // Não Passa!
            System.out.println("Pessoa - Idade:"+pessoa.getIdade());
        }
        catch (PropertyVetoException e) {
            System.out.println("Erro:"+e.getMessage()+" Valor:"+e.getPropertyChangeEvent().getNewValue());
        }

        try {
            pessoa.setIdade(25); // Passa!
            System.out.println("Pessoa - Idade:"+pessoa.getIdade());
        }
        catch (PropertyVetoException e) {
            System.out.println("Erro:"+e.getMessage()+" Valor:"+e.getPropertyChangeEvent().getNewValue());
        }
    }
}
