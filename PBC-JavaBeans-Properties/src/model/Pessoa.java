package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public class Pessoa {

    private String nome;
    private int idade;
    private String email;

    private PropertyChangeSupport changeSupport=new PropertyChangeSupport(this);
    private VetoableChangeSupport vetoSupport=new VetoableChangeSupport(this);

    public void addPropertyVetoableChangeListener(VetoableChangeListener ouvinte){
        vetoSupport.addVetoableChangeListener(ouvinte);
    }

    public void removePropertyVetoableChangeListener(VetoableChangeListener ouvinte){
        vetoSupport.removeVetoableChangeListener(ouvinte);
    }



    public void addPropertyChangeListener(PropertyChangeListener ouvinte){
        changeSupport.addPropertyChangeListener(ouvinte);
    }

    public void removePropertyChangeListener(PropertyChangeListener ouvinte){
        changeSupport.removePropertyChangeListener(ouvinte);
    }


    public Pessoa() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {

        String oldValue=this.nome;

        this.nome = nome;

        changeSupport.firePropertyChange("nome",oldValue,this.nome);
    }


    public void setIdade(int idade) throws PropertyVetoException {

        int oldIdade=this.idade;

        vetoSupport.fireVetoableChange("idade",oldIdade,idade);

        this.idade = idade;

    }


    public int getIdade() {
        return idade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

