package log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Log {

    private String classe;
    private String propriedade;
    private Object anterior;
    private Object atual;
    private long momento;

    public Log(String classe, String propriedade, Object anterior, Object atual) {
        this.classe = classe;
        this.propriedade = propriedade;
        this.anterior = anterior;
        this.atual = atual;
        this.momento = Calendar.getInstance().getTimeInMillis();
    }

    @Override
    public String toString() {
        return "Log{" +
                "classe='" + classe + '\'' +
                ", propriedade='" + propriedade + '\'' +
                ", anterior=" + anterior +
                ", atual=" + atual +
                ", momento=" + formataData(momento) +
                '}';
    }

    public String formataData(long data){
        SimpleDateFormat formatador=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatador.format(new Date(data));
    }

}
