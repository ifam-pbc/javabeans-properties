package log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LogManager {

    private List<Log> logs = new ArrayList<Log>();

    public void addLog(String classe, String propriedade, Object anterior, Object atual) {
        logs.add(new Log(classe, propriedade, anterior, atual));
    }

    public List<Log> getLogs() {
        List<Log> novaLista=new ArrayList<Log>();
        novaLista.addAll(logs);
        return novaLista;
    }


}
