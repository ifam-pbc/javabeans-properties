package ouvintes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

public class VetoOuvinte implements VetoableChangeListener {
    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {

        if(evt.getPropertyName().equals("idade")){
            Integer idade = (Integer) evt.getNewValue();
            if((idade<0) || (idade>150)){
                throw new PropertyVetoException("Deve estar entre 0 a 150 anos!",evt);
            }
        }

    }
}
