package ouvintes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import log.Log;
import log.LogManager;

public class ChangeOuvinte implements PropertyChangeListener {

    LogManager logManager=new LogManager();

    @Override
    public void propertyChange(PropertyChangeEvent evt) {



        logManager.addLog(evt.getSource().getClass().getName(),
                evt.getPropertyName(),
                evt.getOldValue(),
                evt.getNewValue());

    }

    public List<Log> getLogList(){

        List<Log> novaLista=new ArrayList<Log>();

        novaLista.addAll(logManager.getLogs());

        return novaLista;

    }
}
